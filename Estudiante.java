package Laboratorio1;

/**
 *
 * Clase: Estudiante.java<br>
 * Objetivo: Clase que representa a un estudiante con atributos básicos.<br>
 * Asignatura: POO.
 * @version 1.0 21/07/2005
 * @author William Mendoza Rodriguez
 *
 */

public class Estudiante{
	
	/*----Atributos----*/
 
	/** Nombre del Estudiante.*/			private String nombre;				
	/** Número de identificación del Estudiante.*/	private long id;					
	/** Año de nacimiento.*/			private int anio;					
	/** Número de mes en que nació.*/		private int mes;					
	/** Número de día en que nació.*/		private int dia;					
	/** Edad del estudiante.*/			private int edad;
	/** Día de ingreso del estudiante.*/		private int diaIngreso;
	/** Mes de ingreso del estudiante.*/		private int mesIngreso;
	/** Año de ingreso del estudiante.*/		private int anioIngreso;
	/** Semestre actual del estudiante.*/		private int semestre;
	/** Vector de Asignaturas que cursa (cinco).*/	private String[] asignaturas = new String[5];	

	/*----Métodos constructores----*/
/*
	/**
	 *
	 * Constructor de la Clase Estudiante.<br>
	 * Solo de asignación de memoria.
	 *
	 *

	public Estudiante(){};
*/
	/**
	 *
	 * Crea un estudiante con todos sus parámetros vacíos.<br>
	 *
	 */

	public Estudiante(){
		nombre = "";
		id = 0;
		anio = 0;
		mes = 0;
		dia = 0;
		edad = 0;
		diaIngreso = 0;
		mesIngreso = 0;
		anioIngreso = 0;

	}

	/**
	 *
	 * Crea un estudiante con el parámetro "nombre" para inicializacón de este.<br>
	 *
	 * @param nombre nombre del estudiante.
	 *
	 */

	public Estudiante (String nombre){
		this.nombre = nombre;
	}

	/**
	 *
	 * Crea un estudiante con el parámetro "nombre" y "numeroId" para la inicialización de este.<br>.
	 *
	 * @param nombre Nombre del estudiante.
	 * @param id Número de ID de estudiante.
	 *
	 */
   
	public Estudiante (String nombre, int id){
		this.nombre = nombre;
		this.id = id;
	}
	

	/**
	 * Crea un estudiante con los parámetros "nombre", "id", "anio", "mes", "dia", "edad", "diaIngreso", "mesIngreso", "anioIngreso".<br>
	 *
	 * @param nombre Nombre del estudiante.
	 * @param id ID del estudiante.
	 * @param anio Año de nacimiento del estudiante.
	 * @param mes Mes de nacimiento del estudiante.
	 * @param dia Día de nacimiento del estudiante.
	 * @param edad Edad del estudiante.
	 * @param diaIngreso Día de ingreso del estudiante.
	 * @param mesIngreso Mes de ingreso del estudiante.
	 * @param anioIngreso Año de ingreso del estudiante.
	 * @param semestre Semestre del estudiante.
	 *
	 */
	
	public Estudiante (String nombre, int id, int anio, int mes, int dia, int edad, int diaIngreso, int mesIngreso, int anioIngreso, int semestre){
		this.nombre = nombre;
		this.id = id;
		this.anio = anio;
		this.mes = mes;
		this.dia = dia;
		this.edad = edad;
		this.diaIngreso = diaIngreso;
		this.mesIngreso = mesIngreso;
		this.anioIngreso = anioIngreso;
		this.semestre();
	}

	/*----Métodos get & set----*/

	/**
	 *
	 * Recupera el nombre de este estudiante.<br>
	 * 
	 * @return Nombre del estudiante.
	 *
	 */

	public String getNombre() { 
		return this.nombre; 
	}
    
	/**
	 *
	 * Modifica el nombre del estudiante.<br>
	 *
	 * @param nombre Nombre de este estudiante.
	 * 
	 */

	public void setNombre (String nombre) { 
		this.nombre = nombre; 
	}
   
	/**
	 * 
	 * Recupera el ID del estudiante.<br>
	 * 
	 * @return ID del estudiante.
	 * 
	 */

	public long getId() {
		return this.id; 
	}
   
	/**
	 *
	 * Modifica el ID del estudiante.<br>
	 * 
	 * @param id ID del estudiante.
	 *
	 */
	
	public void setId (long id) { 
		this.id = id; 
	}
   
	/**
	 *
	 * Recupera el año de nacimiento del estudiante.<br>
	 * 
	 * @return Año de nacimiento del estudiante.
	 *
	 */
	
	public int getAnio() {
		return this.anio; 
	}

	/**
	 *
	 * Modifica el año de nacimiento del estudiante.<br>
	 *
	 * @param anio Año de nacimiento del estudiante.
	 *
	 */

	public void setAnio(int anio){
		this.anio = anio;
	}

	/**
	 *
	 * Recupera el mes de nacimiento del estuidante.<br>
	 *
	 * @return Mes de nacimiento del estudiante.
	 *
	 */
	
	public int getMes() {
		return this.mes; 
	}

	/**
	 *
	 * Modifica el mes de nacimiento del estudiante.<br>
	 *
	 * @param mes Mes de nacimiento del estudiante.
	 *
	 */

	public void setMes(int mes){
		this.mes = mes;
	}
	    
	/**
	 *
	 * Recupera el día de nacimiento del estudiante.<br>
	 *
	 * @return Día de nacimiento del estudiante.
	 *
	 */

	public int getDia() {
		return this.dia; 
	}
    
	/**
	 *
	 * Modifica la fecha de nacimiento del estudiante.<br>
	 *
	 * @param dia Día de nacimiento del estudiante.
	 * @param mes Mes de nacimiento del estudiante.
	 * @param anio Año de nacimiento del estudiante.
	 *
	 */

	public void setFechaNacimiento(int dia, int mes, int anio){
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
	}
    
	/**
	 *
	 * Recupera la fecha de nacimiento del estudiante.<br>
	 *
	 * @return Fecha de nacimiento del estudiante.
	 *
	 */

	public String getFechaNacimiento(){
		return this.getDia() +"/"+ this.getMes() +"/"+ this.getAnio(); 
	}

	/**
	 *
	 * Modifica la edad del estudiante.<br>
	 *
	 * @param edad Edad del estudiante.
	 *
	 */

	public void setEdad(int edad){
		this.edad = edad;
	}

	/**
	 *
	 * Recupera la edad del estudiante.<br>
	 *
	 * @return Edad del estudiante.
	 *
	 */

	public int getEdad(){
		return this.edad;
	}

	/**
	 *
	 * Modifica el semestre del estudiante.<br>
	 *
	 * @param semestre Semestre del estudiante.
	 *
	 */

	public void setSemestre(int semestre){
		this.semestre = semestre;
	}

	/**
	 *
	 * Recupera el semestre del estudiante.<br>
	 *
	 * @return Semestre del estudiante.
	 *
	 */

	public int getSemestre(){
		return this.semestre;
	}

	/**
	 *
	 * Recupera las asignaturas del estudiante.<br>
	 *
	 * @return Asignaturas del estudiante.
	 *
	 */
	
	public String getAsignaturas(){
		String s="";
		for(int i = 0; i <= 4; i++){
			s += "Asignatura "+(i+1)+": " +asignaturas[i]+ ".\n";
		}
		return s;
	}

	/**
	 *
	 * Modifica las asignaturas del estudiante.<br>
	 *
	 * @param asig1 Primera asignatura.
	 * @param asig2 Segunda asignatura.
	 * @param asig3 Tercera asignatura.
	 * @param asig4 Cuarta asignatura.
	 * @param asig5 Quinta asignatura.
	 *
	 */

	public void asignaturas(String asig1, String asig2, String asig3, String asig4, String asig5){
		asignaturas[0] = asig1;
		asignaturas[1] = asig2;
		asignaturas[2] = asig3;
		asignaturas[3] = asig4;
		asignaturas[4] = asig5;
	}

	/**
	 *
	 * Recupera el día de ingreso del estudiante.<br>
	 *
	 * @return Día de ingreso del estudiante.
	 *
	 */

	public int getDiaIngreso(){
		return this.diaIngreso;
	}

	/**
	 *
	 * Modifica el día de ingreso del estudiante.<br>
	 *
	 * @param diaIngreso Día de ingreso del estudiante.
	 *
	 */

	public void setDiaIngreso(int diaIngreso){
		this.diaIngreso = diaIngreso;
	}

	/**
	 *
	 * Recupera el mes de ingreso del estudiante.<br>
	 *
	 * @return Mes de ingreso del estudiante.
	 *
	 */

	public int getMesIngreso(){
		return this.mesIngreso;
	}

	/**
	 *
	 * Modifica el mes de ingreso del estudiante.<br>
	 *
	 * @param mesIngreso Mes de ingreso del estudiante.
	 *
	 */

	public void setMesIngreso(int mesIngreso){
		this.mesIngreso = mesIngreso;
	}
	
	/**
	 *
	 * Recupera el año de ingreso del estudiante.<br>
	 *
	 * @return Año de ingreso del estudiante.
	 *
	 */

	public int getAnioIngreso(){
		return this.anioIngreso;
	}

	/**
	 *
	 * Modifica el año de ingreso del estudiante.<br>
	 *
	 * @param anioIngreso Año de ingreso del estudiante.
	 *
	 */

	public void setAñoIngreso(int anioIngreso){
		this.anioIngreso = anioIngreso;
	}

	/**
	 *
	 * Recupera la fecha de ingreso de estuidante.<br>
	 *
	 * @return Fecha de ingreso del estudiante.
	 *
	 */

	public String getFechaIngreso(){
		return "Fecha ingreso: " +this.getDiaIngreso()+ "/" +this.getMesIngreso()+ "/" +this.getAnioIngreso()+ ".";
	}

	/**
	 *
	 * Modifica la fecha de ingreso del estudiante.<br>
	 *
	 * @param diaIngreso Día de ingreso del estudiante.
	 * @param mesIngreso Mes de ingreso del estudiante.
	 * @param anioIngreso Año de ingreso del estudiante.
	 *
	 */

	public void setFechaIngreso(int diaIngreso, int mesIngreso, int anioIngreso){
		this.diaIngreso = diaIngreso;
		this.mesIngreso = mesIngreso;
		this.anioIngreso = anioIngreso;
	}


	/**
	 *
	 * Calcula la edad del estudiante en años cumplidos.<br>
	 * 
	 */

	public void edad(){
		int diaActual = 28;
		int mesActual = 8;
		int anioActual = 2020;
		

		if(mesActual > this.getMes() || (mesActual == this.getMes() && diaActual >= this.getDia())){
			this.edad = anioActual - anio;
		}else{
			this.edad = anioActual - anio - 1;
		}

		/*
		if(mes < mesActual || (mes == mesActual && dia <= diaActual)){
			this.edad = anioActual - anio;
		}
		if(mes > mesActual || (mes == mesActual && dia >= diaActual)){
			this.edad = anioActual - anio - 1;
		}
		*/

		/*
		if(mes > mesActual){
			this.edad = anio-anioac;
		}else if(mes == mesnac){
			if (dia >= dianac){
				this.edad = anio-anioac;
			}else{
		       		this.edad = anio-anioac-1;
			}
			if(mes<mesnac){
				this.edad = anio-anioac-1;
			}
		*/
	}
    
	/**
	*
	* Recupera toda la información del estudiante.<br>
	* 
	* @return Toda la información del estudiante.
	*
	*/

	public String toString(){ 
		String s = "";

		s += "Nombre:			" +this.getNombre()+ ".\n";
		s += "ID:			" +this.getId()+ ".\n";
		s += "Fecha de nacimiento:	" +this.getDia()+ "/" +this.getMes()+ "/" +this.getAnio()+ ".\n";
		s += "Semestre:			" +this.getSemestre() + ".\n";
		s += "Edad:			" +this.getEdad()+ ".";
		return s;
	}

	/**
	 *
	 * Calcula el smestre del estudiante.<br>
	 *
	 */

	public void semestre(){
		int diaActual = 27;
		int mesActual = 8;
		int anioActual = 2020;

		if(this.mesIngreso <= 6){
			this.semestre = (anioActual - this.getAnioIngreso() + 1) * 2;
		}else{
			this.semestre = (anioActual - this.getAnioIngreso() + 1) * 2 - 1;
		}
	}
}
