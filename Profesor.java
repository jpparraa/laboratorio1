package Laboratorio1;

/**
 *
 * Clase Profesor.java<br>
 * Objetivo: Clase que representa a un profesor con atributos básicos.<br>
 * Asignatura: POO<br>
 * @version 0.1 28/08/2020
 * @author Juan Parra
 *
 */

public class Profesor{
	
	/*----Atributos----*/

	/** Número de identificación del profesor.*/	private long identificacion;
	/** Nombre del profesor.*/			private String nombre;
	/** Facultad del profesor.*/			private String facultad;
	/** Asignatura que imparte el profesor.*/	private String asignatura;

	/*----Métodos constructores----*/

	/**
	 *
	 * Cronstruye un objeto de tipo "Profesor".<br>
	 * Inicializa todos sus atributos.
	 *
	 */

	public Profesor(){
		this.identificacion = 0;
		this.nombre = "";
		this.facultad = "";
		this.asignatura = "";
	}

	/**
	 *
	 * Construye un objeto de tipo "Profesor".<br>
	 * Inicializa todos sus atributos.
	 *
	 * @param nombre Nombre del profesor.
	 *
	 */
	
	public Profesor(String nombre){
		this.identificacion = 0;
		this.nombre = nombre;
		this.facultad = "";
		this.asignatura = "";
	}

	/**
	 *
	 * Construye un objeto de tipo "Profesor".<br>
	 * Inicializa todos sus atributos.
	 *
	 * @param identificacion Número de identificación del profesor.
	 * @param nombre Nombre del profesor.
	 *
	 */

	public Profesor(int identificacion, String nombre){
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.facultad = "";
		this.asignatura = "";
	}

	/**
	 *
	 * Construte un objeto de tipo "Profesor".<br>
	 * Inicializa todos sus atributos.
	 *
	 * @param identificacion Número de identifiación del profesor.
	 * @param nombre Nombre del profesor.
	 * @param facultad Nombre de la facultad del profesor.
	 * @param asignatura Asignatura que imparte el profesor.
	 *
	 */

	public Profesor(int identificacion, String nombre, String facultad, String asignatura){
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.facultad = facultad;
		this.asignatura = asignatura;
	}

	/*----Métodos analizadores (get)----*/

	/**
	 *
	 * Recupera el ID del profesor.<br>
	 *
	 * @return Número de identificación del profesor.
	 *
	 */

	public long getIdentificacion(){
		return this.identificacion;
	}

	/**
	 *
	 * Recupera el nombre del profesor.<br>
	 *
	 * @return Nombre del profesor.
	 *
	 */

	public String getNombre(){
		return this.nombre;
	}

	/**
	 *
	 * Recupera la facultad del profesor.<br>
	 *
	 * @return Facultad del profesor.
	 *
	 */

	public String getFacultad(){
		return this.facultad;
	}

	/**
	 *
	 * Recupera la asignatura del profesor.<br>
	 *
	 * @return Asignatura del profesor.
	 *
	 */

	public String getAsignatura(){
		return this.asignatura;
	}

	/**
	 *
	 * Recupera la información del profesor.<br>
	 *
	 * @return ID, nombre, facultad y asignatura del profesor.
	 *
	 */

	public String toString(){
		String s = "";

		s += "Identificación		: " +this.getIdentificacion()+ ".\n";
		s += "Nombre			: " +this.getNombre()+ ".\n";
		s += "Facultad			: " +this.getFacultad()+ ".\n";
		s += "Asignatura		: " +this.getAsignatura()+ ".\n";

		return s;
	}

	/*----Métodos modificadores (set)----*/

	/**
	 *
	 * Modifica el ID del profesor.<br>
	 *
	 * @param identificacion Número de identificación del profesor.
	 *
	 */

	public void setIdentificacion(long identificacion){
		this.identificacion = identificacion;
	}

	/**
	 *
	 * Modifica el nombre del profesor.<br>
	 *
	 * @param nombre Nombre del profesor.
	 *
	 */

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	/**
	 *
	 * Modifica la facultad del profesor.<br>
	 *
	 * @param facultad Facultad del profesor.
	 *
	 */

	public void setFacultad(String facultad){
		this.facultad = facultad;
	}

	/**
	 *
	 * Modifica la asignatura del profesor.<br>
	 *
	 * @param asignatura Asignatura del profesor.
	 *
	 */

	public void setAsignatura(String asignatura){
		this.asignatura = asignatura;
	}

	/**
	 *
	 * Modifica todos los datos del profesor.<br>
	 *
	 * @param identificacion Número de identificación del profesor.
	 * @param nombre Nombre del profesor.
	 * @param facultad Facultad del profesor.
	 * @param asignatura Asignatura del profesor.
	 *
	 */

	public void setProfesor(long identificacion, String nombre, String facultad, String asignatura){
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.facultad = facultad;
		this.asignatura = asignatura;
	}
}
