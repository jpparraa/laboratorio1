package Laboratorio1;

/**
 *
 * CLASE: TestEstudiante.java<br>
 * OBJETIVO: Esta es una clase para probar la clase "Estudiante".<br>
 * ASIGNATURA: POO<br>
 * @version 1.0 21/07/2005
 * @author William Mendoza Rodriguez
 *
 */
 
public class TestEstudiante{


	/*----Implementación de la clase estudiante----*/

	/**
	 * 
	 * Un test de la clase estudiante. Retorna un string.<br>
	 * 
	 * Precondicion: TRUE <br>
	 * 
	 * @return Resultado del test como un String.
	 * 
	 * @exception Exception si cualquier excepción es lanzada.
	 * 
	 */
	
	public static String testEstudiante() throws Exception{
		String s = "";
		long id = 101056;
	/*
		Estudiante colombiano = new Estudiante("Catalina");
		Estudiante mexicano = new Estudiante("Juan en Ruso", 101711);
		Estudiante nacional = new Estudiante();
		Estudiante extranjero;
	
		colombiano.setId(id);
		nacional = colombiano;
		extranjero = mexicano;
		extranjero.setNombre("Ivan");
	
		s += colombiano.toString() + "\n\n\n";
		s += mexicano.toString() + "\n\n\n";
		s += nacional.toString() + "\n\n\n";
		s += extranjero.toString() + "\n\n\n";
	*/
		//Estudiante que ingresó desde el principo del año y no ha cumplido años este año
		Estudiante e1 = new Estudiante();
		e1.setNombre("Juan");
		e1.setId(1);
		e1.setFechaNacimiento(11, 11, 2000);
		e1.edad();
		e1.asignaturas("a", "b", "c", "d", "e");
		e1.setFechaIngreso(2, 2, 2019);
		e1.semestre();
		
		s += e1.toString() + "\n";
		s += e1.getAsignaturas() + "\n\n";

		//Estudiante que ingresó desde mitad de año y ya cumplió años este año
		Estudiante e2 = new Estudiante();
		e2.setNombre("Pablo");
		e2.setId(2);
		e2.setFechaNacimiento(11, 5, 2000);
		e2.edad();
		e2.asignaturas("a", "b", "c", "d", "e");
		e2.setFechaIngreso(2, 7, 2019);
		e2.semestre();
		
		s += e2.toString() + "\n";
		s += e2.getAsignaturas();

		return s;
	}

	/*----Programa principal----*/

	/**
	 *
	 * Método main para el test.<br>
	 *
	 * @param args[] Argumentos iniciales del programa.
	 *
	 * @exception Exception si cualquier excepción es lanzada.
	 *
	 */

	public static void main(String args[]) throws Exception{
		System.out.println("====Estudiantes====\n");
		System.out.println(testEstudiante());
	}
}
