package Laboratorio1;

/**
 *
 * CLASE: TestProfesor.java<br>
 * OBJETIVO: Esta es una clase para probar la clase "Profesor".<br>
 * ASIGNATURA: POO<br>
 * @version 1.0 21/07/2005
 * @author William Mendoza Rodriguez
 *
 */

public class TestProfesor{

	/*----Implementación de la clase "Profesor"----*/

	/**
	 * 
	 * Un test de la clase profesor. Retorna un string.<br>
	 * 
	 * Precondicion: TRUE <br>
	 * 
	 * @return Resultado del test como un String.
	 * 
	 * @exception Exception si cualquier excepción es lanzada.
	 * 
	 */

	public static String testProfesor() throws Exception{
		String s = "";

		Profesor p1 = new Profesor();
		Profesor p2 = new Profesor("Juan");
		Profesor p3 = new Profesor(3, "Miguel");
		Profesor p4 = new Profesor(4, "Mario", "Ingeniería", "POO");

		p1.setProfesor(1, "Jerónimo", "Artes", "Diseño");
		
		p2.setIdentificacion(2);
		p2.setFacultad("Ingeniería");
		p2.setAsignatura("Pruebas");

		p3.setFacultad("Artes");
		p3.setAsignatura("Color");

		p4.setNombre("Nicolás");

		s += p1.toString() + "\n"; 

		s += p2.toString() + "\n";

		s += p3.toString() + "\n";

		s += p4.toString() + "\n";

		return s;
	}

	/*----Programa principal----*/

	/**
	 *
	 * Método main para el test.<br>
	 *
	 * @param args[] Argumentos iniciales del programa.
	 *
	 * @exception Exception si cualquier excepción es lanzada.
	 *
	 */

	public static void main(String args[]) throws Exception{
		System.out.println("====Profesores====\n");
		System.out.print(testProfesor());
	}
}
